<p align="center"><img width="200px" src="setup/gui/icon.png" /></p>

# Weather app - Do you need an umbrella today?


[UBports](https://ubports.com) weather app is one of the core apps and provides weather information for your Ubuntu Touch device.
To download visit [weather app at OpenStore](https://open-store.io/app/com.ubuntu.weather). Or build it yourself following instructions below.


# How to build


Read the doc for some general information and guidelines about:

* [app development](http://docs.ubports.com/en/latest/appdev/index.html)

* [contributing](http://docs.ubports.com/en/latest/contribute/index.html)

### API
Weather app currently supports only weather data from [OpenWeatherMap](https://openweathermap.org/).

The OpenWeatherMap service requires an API key.
Visit [OpenWeatherMap - API](https://openweathermap.org/api) for detailed information about the API and how to obtain a personal key. Get your key and place it between the quotation marks for the *"owmKey"* variable in *app/data/keys.js*.

**Do not commit branches with private keys in place! A centrally managed key is injected at build time.**

*Note: Some services may require a paid plan.*

### Dependencies

**DEPENDENCIES ARE NEEDED TO BE INSTALLED TO BUILD AND RUN THE APP**.

A complete list of dependencies for the project can be found in ubuntu-weather-app/debian/control

The following essential packages are also required to develop this app:

* Qt5       - to download and install follow the instructions [here](https://www.qt.io/download)
* intltool  - run  ```sudo apt install intltool```

### Build
To build weather app for Ubuntu Touch devices you do need clickable. Follow install instructions at its repo [here](https://gitlab.com/clickable/clickable). For more information and help visit the [docs](ble.bhdouglass.com/en/latest/getting-started.html).
Once clickable is installed you are ready to go.

1. Fork [weather app at gitlab](https://gitlab.com/ubports/apps/weather-app) into your own namespace. You may need to open an accout with gitlab if not already done.

2. Open a terminal: ctl + alt + t.

3. Clone the repo onto your local machine using git: `git clone https://gitlab.com/ubports/apps/weather-app.git`.

4. Change into weather apps folder: `cd weather-app`.

5. run clickable: `clickable`.

### Translation

Translation of weather app is done using weblate. Goto [weather app on weblate](https://translate.ubports.com/projects/ubports/weather-app/), login, start translating.

To add a new language log into weblate, goto *tools* --> *start new translation*.

## Old Canonical references

* [Home Page](https://developer.ubuntu.com/en/community/core-apps/weather/) (broken)
* [Weather App Wiki](https://wiki.ubuntu.com/Touch/CoreApps/Weather) (broken)
* [Designs](https://developer.ubuntu.com/en/community/core-apps/weather/#design) (broken)
* [Project page at launchpad](https://launchpad.net/ubuntu-weather-app)
